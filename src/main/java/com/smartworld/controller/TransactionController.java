package com.smartworld.controller;

import com.smartworld.model.TransactionInput;
import com.smartworld.model.TransactionOutput;
import com.smartworld.model.TransactionOutputRecord;
import com.smartworld.service.TransactionService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
public class TransactionController {

    /**
     * earnPoint from Admin
     * @param debitWalletId
     * @param points
     * @param timestamp
     * @return
     */
    @PostMapping("/earnPointFromAdmin")
    public ResponseEntity earnPoint(@RequestParam("debitWalletId") String debitWalletId,
                                    @RequestParam("points") Integer points,
                                    @RequestParam(name = "timestamp", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date timestamp) {
        try {
            if (timestamp == null) {
                timestamp = new Date();
            }
            TransactionService.earnPoint(debitWalletId, points, timestamp);
            return ResponseEntity.status(HttpStatus.OK).body("success");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    /**
     * Transfer point from one address to another
     * @param txo
     * @return
     */
    @PostMapping("/transfer")
    public ResponseEntity transfer(@RequestBody TransactionOutput txo) {
        try {
            TransactionService.transfer(txo.getCreditWalletId(), txo.getDebitWalletId(), txo.getPoints(), txo.getTimestamp());
            return ResponseEntity.status(HttpStatus.OK).body("success");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    /**
     * Spend point from address
     * @param address
     * @param txi
     * @return
     */
    @PostMapping("/spend/{address}")
    public ResponseEntity spend(@PathVariable String address,
                                @RequestBody TransactionInput txi) {
        try {
            List<TransactionOutputRecord> result = TransactionService.spend(address, txi.getPoints());
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * return balance by address
     * @param address
     * @return
     */
    @GetMapping("/getBalance")
    public ResponseEntity getBalance(@RequestParam("address") String address) {
        return ResponseEntity.status(HttpStatus.OK).body(TransactionService.getBalance(address));
    }

    /**
     * return all balances from the accounts
     * @return
     */
    @GetMapping("/getBalances")
    public ResponseEntity getBalances() {
        return ResponseEntity.status(HttpStatus.OK).body(TransactionService.getBalances());
    }
}

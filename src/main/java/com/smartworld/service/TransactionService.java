package com.smartworld.service;

import com.smartworld.model.TransactionInput;
import com.smartworld.model.TransactionOutput;
import com.smartworld.model.TransactionOutputRecord;

import java.util.*;

import static com.smartworld.PointSystemApplication.*;
import static com.smartworld.util.Util.getUUIDString;

public class TransactionService {
    public static Integer getBalance(String address) {
        if (!TXO_LIST_MAP.containsKey(address)) return 0;

        Integer balance = 0;
        List<TransactionOutput> transactionOutputList = TXO_LIST_MAP.get(address);
        for (TransactionOutput txo : transactionOutputList) {
            balance += txo.getPoints();
        }

        if (TXI_LIST_MAP.containsKey(address)) {
            List<TransactionInput> transactionInputList = TXI_LIST_MAP.get(address);
            for (TransactionInput txi : transactionInputList) {
                balance -= txi.getPoints();
            }
        }

        return balance;
    }

    public static void earnPoint(String debitWalletId, Integer points, Date timestamp) throws Exception {
        if (points <= 0) {
            throw new Exception("points must be bigger than 0");
        }

        // Step1: create transaction output record
        TransactionOutput txo = new TransactionOutput(getUUIDString(), "SYSTEM_SUPER_USER", debitWalletId, points, timestamp);

        // Step2: process txo
        if (TXO_LIST_MAP.containsKey(debitWalletId)) {
            TXO_LIST_MAP.get(debitWalletId).add(txo);
        } else {
            List<TransactionOutput> transactionOutputList = new ArrayList<>();
            transactionOutputList.add(txo);
            TXO_LIST_MAP.put(debitWalletId, transactionOutputList);
        }

        TXO_LIST_MAP.get(debitWalletId).sort(Comparator.comparing(TransactionOutput::getTimestamp));
    }

    public static List<TransactionOutputRecord> spend(String address, Integer points) throws Exception {
        if (points <= 0) {
            throw new Exception("points must be bigger than 0");
        }

        if (getBalance(address) < points) {
            throw new Exception("balance not enough");
        }

        // Step1: Create transaction input record
        TransactionInput txi = new TransactionInput(getUUIDString(), points, new Date());
        if (TXI_LIST_MAP.containsKey(address)) {
            TXI_LIST_MAP.get(address).add(txi);
        } else {
            List<TransactionInput> transactionInputList = new ArrayList<>();
            transactionInputList.add(txi);
            TXI_LIST_MAP.put(address, transactionInputList);
        }

        TXI_LIST_MAP.get(address).sort(Comparator.comparing(TransactionInput::getTimestamp));
        return getTXOListFromTXI(address, txi.getTxiId());
    }

    public static void transfer(String creditWalletId, String debitWalletId, Integer points, Date timestamp) throws Exception {
        if (points <= 0) {
            throw new Exception("points must be bigger than 0");
        }
        // Step1: check the sender's balance is bigger than points
        if (getBalance(creditWalletId) < points) {
            throw new Exception("sender's balance not enough");
        }

        // Step2: create transaction output record
        TransactionOutput txo = new TransactionOutput(getUUIDString(), creditWalletId, debitWalletId, points, timestamp);


        // Step3: process sender
        TransactionInput txi = new TransactionInput(getUUIDString(), points, timestamp);
        if (TXI_LIST_MAP.containsKey(creditWalletId)) {
            TXI_LIST_MAP.get(creditWalletId).add(txi);
        } else {
            List<TransactionInput> transactionInputList = new ArrayList<>();
            transactionInputList.add(txi);
            TXI_LIST_MAP.put(creditWalletId, transactionInputList);
        }

        TXI_LIST_MAP.get(creditWalletId).sort(Comparator.comparing(TransactionInput::getTimestamp));


        // Step4: process recipient
        if (TXO_LIST_MAP.containsKey(debitWalletId)) {
            TXO_LIST_MAP.get(debitWalletId).add(txo);
        } else {
            List<TransactionOutput> transactionOutputList = new ArrayList<>();
            transactionOutputList.add(txo);
            TXO_LIST_MAP.put(debitWalletId, transactionOutputList);
        }

        TXO_LIST_MAP.get(debitWalletId).sort(Comparator.comparing(TransactionOutput::getTimestamp));
    }

    public static Map<String, Integer> getBalances() {
        Map<String, Integer> balanceMap = new HashMap<>();
        for (String address : TXO_LIST_MAP.keySet()) {
            balanceMap.put(address, getBalance(address));
        }
        return balanceMap;
    }

    public static List<TransactionOutputRecord> getTXOListFromTXI(String address, String txiId) {
        List<TransactionOutputRecord> result = new ArrayList<>();

        List<TransactionInput> transactionInputList = TXI_LIST_MAP.get(address);
        int beforeSpendSum = 0;
        int currentSpend = 0;
        for (TransactionInput transactionInput : transactionInputList) {
            if (transactionInput.getTxiId().equals(txiId)) {
                currentSpend = transactionInput.getPoints();
                break;
            }
            beforeSpendSum += transactionInput.getPoints();
        }

        int beforeGetSum = 0;
        int getSum = 0;
        List<TransactionOutput> transactionOutputList = TXO_LIST_MAP.get(address);
        for (TransactionOutput transactionOutput : transactionOutputList) {
            getSum += transactionOutput.getPoints();
            if (getSum > beforeSpendSum) {
                result.add(new TransactionOutputRecord(transactionOutput.getCreditWalletId(), transactionOutput.getPoints()));
            } else {
                beforeGetSum = getSum;
            }
            if (getSum >= beforeSpendSum + currentSpend) {
                break;
            }
        }

        int indexFirstTXI = 0;
        int indexLastTXI = result.size() - 1;

        result.get(indexFirstTXI).setPoints(result.get(indexFirstTXI).getPoints() - (beforeSpendSum - beforeGetSum));
        result.get(indexLastTXI).setPoints(result.get(indexLastTXI).getPoints() - (getSum - beforeSpendSum - currentSpend));
        return result;
    }
}

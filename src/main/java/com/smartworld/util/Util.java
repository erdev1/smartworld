package com.smartworld.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

public class Util {
    public static String getUUIDString() {
        return UUID.randomUUID().toString();
    }
}

package com.smartworld;

import com.smartworld.model.TransactionInput;
import com.smartworld.model.TransactionOutput;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class PointSystemApplication {
    // TXI List Map (address -> TXI list)
    public static final Map<String, List<TransactionInput>> TXI_LIST_MAP = new HashMap<>();

    // TXO List Map (address -> TXO list)
    public static final Map<String, List<TransactionOutput>> TXO_LIST_MAP = new HashMap<>();

    public static void main(String[] args) {
        SpringApplication.run(PointSystemApplication.class, args);
    }
}

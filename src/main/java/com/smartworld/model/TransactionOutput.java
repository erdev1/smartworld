package com.smartworld.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * Author: Erdoan
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransactionOutput {
    private String txoId;
    private String creditWalletId;
    private String debitWalletId;
    private Integer points;
    private Date timestamp;
}

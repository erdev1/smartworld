# SmartWorld Home Test

In this project, I used Springboot with Maven. It will run on 8080 port on local.

## Run Server
If you have Java, Maven and IDEA in local, then you can open the project by using IDEA.

Otherwise, can select to run by Docker by below command:

`docker-compose -f docker-compose.yml up -d --build`

Note: To run by using docker, you need to have docker environment and docker-compose as well. And it will take bit long time at the first run to download maven repositories.

## Technical Keys
In this point system for the reporting and auditing purposes, 
I made the two models which show disbursed transaction(`TransactionInput` class) and 
earning transaction(`TransactionOutput` class).

This idea is from blockchain network, actually in blockchain network use `UTXO` to calculate the balance of the account.

In `com.smartworld.PointSystemApplication`, I defined `TXI_LIST_MAP` and `TXO_LIST_MAP` which saves
the `TransactionInput` list and `TransactionOutput` list order by timestamp, according to the address.

## At a Glance!
There are five APIs, and after running the project, you can see the APIs by visiting `http://localhost:8080/swagger-ui.html`.

1. `/earnPointFromAdmin`
By calling this API, we can send the points to specific address. Need to pass `target` address and `points`. The `timestamp` is not required, and if don't pass it then will take current time.

![img.png](images/01.earnPointFromAdmin.png)
   
2. `/transfer`
You need to pass the json body, which mentioned in the guide documents like below:
```
   {
   "creditWalletId": "8c18b5bf-0171-4918-a611-bde754382f7a",
   "debitWalletId": "363a3f19-7fa9-4e34-851d-6e42ef92a285",
   "points": 1000,
   "timestamp": "2021-11-02T14:00:00Z"
   }
```
![img.png](images/02.transferPoint.png)
If sender's balance not enough, it will respond 400 error with error message.

3. `/spend`
Need to pass the address and json body to call this API. Json body like below:
```
{
"points": 700
}
```
And It will return the points balance route.
```
[
  {
    "creditWalletId": "d5af01f0-515a-4834-ab4e-a2f54aeaedbf",
    "points": 300
  },
  {
    "creditWalletId": "d5af01f0-515a-4834-ab4e-a2f54aeaedbf",
    "points": 300
  },
  {
    "creditWalletId": "d5af01f0-515a-4834-ab4e-a2f54aeaedbf",
    "points": 100
  }
]
```
![img.png](images/03.spend-1.png)
![img_1.png](images/03.spend-2.png)

4. `/getBalances`
You can get all address->balance pairs by calling this API.
```
{
"d5af01f0-515a-4834-ab4e-a2f54aeaedbf": 14200,
"363a3f19-7fa9-4e34-851d-6e42ef92a285": 3000
}
```

![img.png](images/04.getBalances.png)
